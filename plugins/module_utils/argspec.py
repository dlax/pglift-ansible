import json
import shlex
import subprocess
from pathlib import Path
from typing import Any, Dict, Optional, Sequence

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.dalibo.pglift.plugins.module_utils.importcheck import (
    check_required_libs,
)

with check_required_libs():
    import yaml


def build_doc(
    path: Path,
    *,
    instance_reference: bool = False,
    include_only: Optional[Sequence[str]] = None,
) -> str:
    with path.open() as f:
        argspec = json.load(f)
    if instance_reference:
        argspec["options"]["instance"] = {
            "required": True,
            "description": ["Instance name."],
            "type": "str",
        }
    if include_only is not None:
        argspec["options"] = {
            k: v for k, v in argspec["options"].items() if k in include_only
        }
    return yaml.safe_dump(argspec, sort_keys=False)  # type: ignore[no-any-return]


def argspec_from_cli(
    obj_name: str,
    instance_reference: bool = False,
    include_only: Optional[Sequence[str]] = None,
) -> Any:
    """Get 'options' and 'return values' definitions of 'obj_name' by calling pglift CLI.

    If 'include_only' is not None, only include listed fields.
    If 'instance_reference' is True, add an extra "instance" field.
    """
    proc = subprocess.run(
        ["pglift", obj_name, "--ansible-argspec"], stdout=subprocess.PIPE, check=True
    )
    argspec = json.loads(proc.stdout)

    if instance_reference:
        argspec["options"]["instance"] = {
            "required": True,
            "description": ["Instance name."],
            "type": "str",
        }

    if include_only is not None:
        argspec["options"] = {
            k: v for k, v in argspec["options"].items() if k in include_only
        }

    return argspec


def exec_apply_cmd(
    obj_type: str, module: AnsibleModule, instance_name: Optional[str] = None
) -> Dict[str, Any]:
    """Execute pglift command for a specific object (instance, role or database)"""
    cmd = ["pglift", "--log-level=debug", "--non-interactive", obj_type]
    if instance_name is not None:
        cmd += ["-i", instance_name]
    cmd += ["apply", "--output-format=json", "--file", "-"]
    if module.check_mode:
        cmd.append("--dry-run")

    rc, stdout, stderr = module.run_command(cmd, data=json.dumps(module.params))

    for errline in stderr.splitlines():
        module.log(errline)

    if rc != 0:
        module.fail_json(
            cmd=shlex.join(cmd),
            rc=rc,
            stdout=stdout,
            stderr=stderr,
            msg=f"'pglift {obj_type} apply' command failed",
        )

    r = json.loads(stdout)
    assert isinstance(r, dict)
    return r
