ANSIBLE_METADATA = {
    "metadata_version": "0.1",
    "status": ["preview"],
    "supported_by": "community",  # XXX
}

DOCUMENTATION = """
---
module: instance

short_description: Create, update and delete a PostgreSQL server instance

description:
- "Manage a PostgreSQL server instance"
extends_documentation_fragment:
- dalibo.pglift.instance_doc

author:
- Dalibo (@dalibo)
"""

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.dalibo.pglift.plugins.module_utils.argspec import (
    argspec_from_cli,
    exec_apply_cmd,
)
from ansible_collections.dalibo.pglift.plugins.module_utils.versioncheck import (
    check_pglift_version,
)


def run_module() -> None:
    argspec = argspec_from_cli("instance")["options"]
    module = AnsibleModule(argument_spec=argspec, supports_check_mode=True)

    check_pglift_version(module)

    exec_res = exec_apply_cmd("instance", module)
    module.exit_json(changed=(exec_res["change_state"] is not None), **exec_res)


def main() -> None:
    run_module()


if __name__ == "__main__":
    main()
