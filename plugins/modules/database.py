ANSIBLE_METADATA = {
    "metadata_version": "0.1",
    "status": ["preview"],
    "supported_by": "community",  # XXX
}

DOCUMENTATION = """
---
module: database

short_description: Create, update and delete databases of a PostgreSQL server instance

description:
- "Manage databases of a PostgreSQL server instance"

extends_documentation_fragment:
- dalibo.pglift.database_doc
"""


RETURN = """
"""

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.dalibo.pglift.plugins.module_utils.argspec import (
    argspec_from_cli,
    exec_apply_cmd,
)
from ansible_collections.dalibo.pglift.plugins.module_utils.versioncheck import (
    check_pglift_version,
)


def run_module() -> None:
    argspec = argspec_from_cli("database", instance_reference=True)["options"]
    module = AnsibleModule(argument_spec=argspec, supports_check_mode=True)

    instance_id = module.params.pop("instance")

    check_pglift_version(module)

    exec_res = exec_apply_cmd("database", module, instance_id)
    module.exit_json(changed=(exec_res["change_state"] is not None), **exec_res)


def main() -> None:
    run_module()


if __name__ == "__main__":
    main()
