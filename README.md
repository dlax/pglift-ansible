# Ansible Collection - dalibo.pglift

## Description

This collection provides modules to configure and manage your PostgreSQL
infrastructure based on [pglift](https://pglift.readthedocs.io/); see the
[tutorial](https://pglift.readthedocs.io/en/latest/tutorials/ansible.html).

## Dependencies

To use this collection, pglift must be installed on the managed machine (the
one hosting the PostgreSQL instances you want to manage with the pglift
Ansible collection). For more information about how to install pglift, you can
read and follow the [pglift
documentation](https://pglift.readthedocs.io/en/latest/install.html).

## Tested with Ansible

4.1

## Included content

### modules

- `instance`: manage pglift (PostgreSQL) instances
- `database`: manage databases in a PostgreSQL instance
- `role`: manage roles in a PostgreSQL instance
- `postgres_exporter`: manage a Prometheus postgres\_exporter bound to a
  PostgreSQL instance
- `dsn_info`: get libpq environment to connect to a PostgreSQL server instance

## Roadmap

FIXME

## More information

General information:

- [Ansible Collection overview](https://github.com/ansible-collections/overview)
- [Ansible User guide](https://docs.ansible.com/ansible/latest/user_guide/index.html)
- [Ansible Developer guide](https://docs.ansible.com/ansible/latest/dev_guide/index.html)
- [Ansible Collections Checklist](https://github.com/ansible-collections/overview/blob/master/collection_requirements.rst)
- [Ansible Community code of conduct](https://docs.ansible.com/ansible/latest/community/code_of_conduct.html)
- [The Bullhorn (the Ansible Contributor newsletter)](https://us19.campaign-archive.com/home/?u=56d874e027110e35dea0e03c1&id=d6635f5420)
- [Changes impacting Contributors](https://github.com/ansible-collections/overview/issues/45)

## Licensing

The code in this repository is developed and distributed under the GNU General
Public License version 3. See LICENSE for details.
